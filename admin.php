<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 2.2 Обработка форм</title>
</head>
<body>
	<h2>Загрузите новый тест</h2>
	<form action="load.php" method="post" enctype="multipart/form-data">
		<label>выберите файл</label>	
		<input type="file" name="file_name">
		<input type="submit" value="Загрузить">
	</form>
	<a href="list.php"> посмотреть список загруженных тестов </a>
	<?php 
	  $test_arr = glob(__dir__ . "/files/" . "*" . "json");
	  if (($test_arr==false)||empty($test_arr)) {
	  	$caption_label = "Очень жаль, но тесты не загружены.";
	  	$file_number = 0;
	  }
	  else {
		   $file_number = count(array_filter($test_arr, "is_file"));
		   $caption_label = "Укажите номер теста: число от 1 до " . $file_number;
		};
		
	?>
	<h2>Пройти тест</h2>
	<form action="test.php" method="GET">
		<label><?= $caption_label ?></label>	
		<?php if ($file_number > 0) {
		?>
			<input type="text" name="test">
			<input type="submit" value="Пройти">
		<?php } 
		?>
	</form>
</body>
</html>