<?php
//проверяем это проверка выполненного теста или загрузка нового и определяем какой тест используется
if (!isset($_POST["submit"])) {
	if (isset($_GET["test"])) {
		$test = $_GET["test"]; 
	}
} else {
	$test = $_POST["test"]; 
};	
//загружаем информацию о тесте
$json_str = file_get_contents("files/" . $test . ".json");
$json_obj = json_decode($json_str,true);
//проверка ответов если это проверка теста
if (isset($_POST["submit"])) {
	$result = true;
	
	foreach ($json_obj as &$question) {
		$result = ($_POST["id".$question["Id"]] == $question["answer"]) && $result;
	
	}
	
	if ($result) {
		$result_header = "Тест пройден";
	} else {
		$result_header = "Тест не пройден";
	};
};
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title> ДЗ 2.2 Тест № <?= $test?> </title>
</head>
<body>
	<h2>Tест <?= $test ?></h2>
	<form action="test.php" method="post">
		<?php
		foreach ($json_obj as &$question) {
			?>	
			<p><?= htmlspecialchars($question["question"]) ?>
				<input type="text" name=<?="id" . $question["Id"] ?> value=<?php if (isset($_POST["submit"])) { echo $_POST["id" . $question["Id"]];}; ?>>
				<?php if (isset($_POST["submit"])) { echo " правильный ответ: ". $question["answer"];}; ?>
			</p>		
				<?php	
			}	
			?>		
			<input type="hidden" name="test" value=<?= $test ?>>
			<?php if (isset($_POST["submit"])) {
				?>
			<h3><?= $result_header ?></h3>
				<?php	
			} else {
				?>
				<input type="submit" name="submit" value="Отправить на проверку">
				<?php	
			}
			?>
	</form>
	<a href="admin.php">Вернуться к форме выбора файла</a>
</body>
</html>
