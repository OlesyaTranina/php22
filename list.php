<?php
$testArr = glob(__dir__ . "/files/". "*.json");
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Задание php 22</title>
</head>
<body>
	<h2>Сейчас загружены тесты</h2>
	<ul>
		<?php
		foreach ($testArr as &$test){
		?>
			<li><?= $test ?></li>
		<?php  }
		?>
	</ul>
	<a href="admin.php">Вернуться к форме выбора файла</a>
</body>
</html>